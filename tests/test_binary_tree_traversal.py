import unittest
from ds import BinaryNode
from ds import BinaryTreeTraversals
from ds import utils


class TestBinaryTressTraversal(unittest.TestCase):

    def test_pre_order(self):
        actual = list()
        BinaryTreeTraversals.pre_order(self.helper(self), actual)
        expected = [0, 1, 3, 4, 2]
        self.assertTrue(utils.compare_two_lists(actual, expected))

    def test_in_order(self):
        actual = list()
        BinaryTreeTraversals.in_order(self.helper(self), actual)
        expected = [3, 4, 1, 0, 2]
        self.assertTrue(utils.compare_two_lists(actual, expected))

    def test_post_order(self):
        actual = list()
        BinaryTreeTraversals.post_order(self.helper(self), actual)
        expected = [2, 0, 1, 4, 3]
        self.assertTrue(utils.compare_two_lists(actual, expected))

    def test_bfs_iteration(self):
        actual = BinaryTreeTraversals.bfs_iteration(self.helper(self))
        expected = [0, 1, 2, 3, 4]
        self.assertTrue(utils.compare_two_lists(actual, expected))

    def test_bfs_recursion(self):
        actual = list()
        a_list = [self.helper(self)]
        BinaryTreeTraversals.bfs_recursion(a_list, actual)
        expected = [0, 1, 2, 3, 4]
        self.assertTrue(utils.compare_two_lists(actual, expected))

    @staticmethod
    def helper(self):
        node0 = BinaryNode.BinaryNode(0)
        node1 = BinaryNode.BinaryNode(1)
        node2 = BinaryNode.BinaryNode(2)
        node3 = BinaryNode.BinaryNode(3)
        node4 = BinaryNode.BinaryNode(4)

        node0.left = node1
        node0.right = node2
        node1.left = node3
        node3.right = node4
        return node0

    #      0
    #     / \
    #    1   2
    #   /
    #  3
    #   \
    #    4


if __name__ == '__main__':
    unittest.main()