import unittest
from ds import utils


class TestUtils(unittest.TestCase):

    def test_compare_two_lists_same(self):
        a = [1, 2, 5]
        b = [1, 2, 5]
        self.assertTrue(utils.compare_two_lists(a,b))

    def test_compare_two_lists_diff(self):
        a = [1, 2, 5]
        b = [1, 5, 2]
        self.assertFalse(utils.compare_two_lists(a,b))

    def test_gcd(self):
        self.assertEqual(5, utils.gcd(25,35))
        self.assertEqual(32, utils.gcd(64,32))
        self.assertEqual(1, utils.gcd(2,5))

    def test_lcm(self):
        self.assertEqual(130, utils.lcm(65,10))
        self.assertEqual(130, utils._lcm([65, 10, 5]))


if __name__ == '__main__':
    unittest.main()
