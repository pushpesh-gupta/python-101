import unittest
from myclasses import Car


class TestCar(unittest.TestCase):

    def test_car(self):
        my_car = Car.Car()
        print("I'm a car!")
        my_car.accelerate()
        my_car.accelerate()
        my_car.accelerate()
        my_car.step()
        my_car.say_state()
        my_car.brake()
        my_car.brake()
        my_car.brake()
        my_car.say_state()


if __name__ == '__main__':
    unittest.main()
