'''
This script turns on webcam of the laptop
and starts capturing the video till we hot 'q' key!!!
'''

import cv2
import time

video = cv2.VideoCapture(0)

count = 1

while True:
    ++count
    check, frame = video.read()

    #print(type(check))
    #print(type(frame))
    #time.sleep(3)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    cv2.imshow("Capturing", gray)

    key = cv2.waitKey(1)
    if key == ord('q'):
        break

print("Number of frames " + count)
video.release()
cv2.destroyAllWindows
