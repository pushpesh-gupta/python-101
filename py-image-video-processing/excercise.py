'''
read all the *.jpg files and save them to 100*100 pixel size.
If they are already 100*100 skip
'''

import cv2
import glob

files = glob.glob("*.jpg")

for file in files:
    print("processing " + file)
    img = cv2.imread(file,0)
    if img.shape[0] == 100 and img.shape[1] == 100:
        print("passed " + file)
        pass
    else:
        new_img = cv2.resize(img, (100,100))
        cv2.imshow("Resized Image", new_img)
        cv2.waitKey(500)
        cv2.destroyAllWindows()
        cv2.imwrite("small_"+file, new_img)
