'''
Run this script with Python 2
For some reason sqlite3 install on this machine does not work with Python 3
'''
import sqlite3

def execute_sql(_sql, params=None):
    conn = sqlite3.connect("lite.db")
    cur = conn.cursor()
    if params is None:
        cur.execute(_sql)
    else:
        cur.execute(_sql,params)
    conn.commit()
    conn.close()

def create_table():
    execute_sql("CREATE TABLE IF NOT EXISTS store(item TEXT, quantity INTEGER, price REAL)")

def insert_data(item, quantity, price):
    execute_sql("INSERT INTO store VALUES(?,?,?)", (item, quantity, price))

def view_all():
    conn = sqlite3.connect("lite.db")
    cur = conn.cursor()
    cur.execute("SELECT * FROM store")
    rows = cur.fetchall()
    conn.close()
    return rows

def delete_all():
    execute_sql("DELETE FROM store")

def delete(item):
    conn = sqlite3.connect("lite.db")
    cur = conn.cursor()
    cur.execute("DELETE FROM store WHERE item = ?", (item,))
    conn.commit()
    conn.close()

def update(item, quantity, price):
    conn = sqlite3.connect("lite.db")
    cur = conn.cursor()
    cur.execute("UPDATE store SET price = ?, quantity = ? WHERE item = ?", (price, quantity, item))
    conn.commit()
    conn.close()

#create_table()
#delete_all()
#insert_data("apple", 10, 10.0)
#insert_data("orange", 20, 20.0)
#delete("banana")
#insert_data("banana", 12, 12.0)
update("orange", 100, 100.0)
print(view_all())
