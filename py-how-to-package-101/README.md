# Example Package

This is a simple example to learn how to package and distribute python package.

It follows https://packaging.python.org/tutorials/packaging-projects/

create virtualenv for python3

pip install setuptools and wheel

To create distribution package run below:

python3 setup.py sdist bdist_wheel
