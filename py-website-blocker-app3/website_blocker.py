# In this exercise we are trying following:
# If it is office hours, add block certain websites
# by adding them to /etc/hosts.
# (instead of modifying real /etc/host we will modify local hosts file, to show python code words)
# To modify real file run, change file to use "etc/hosts" and run below
# sudo python3 website_blocker.py

import time
from datetime import datetime as dt

hosts_file="./hosts"
redirect="127.0.0.1"
websites_to_block=["ndtv.com","www.ndtv.com","new.google.com","www.news.google.com","www.cricbuzz.com","cricbuzz.com"]

while True:
    if dt.now().hour >= 9 and dt.now().hour < 17:
        print("working hours...")
        with open(hosts_file,'r+') as file:
            contents = file.read()
            for website in websites_to_block:
                if website in contents:
                    pass
                else:
                    file.write(redirect + " " + website + '\n')
    else:
        print("non working hours...")
        with open(hosts_file,'r+') as file:
            contents = file.readlines()
            file.seek(0)
            for line in contents:
                if not any(website in line for website in websites_to_block):
                    file.write(line)
            file.truncate()

    time.sleep(5)
