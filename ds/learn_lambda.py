dimensions = {"a": 1,"b": 1,"c": 3}

if len(dimensions.keys()) > 0:
    dims = list(
        map(
            lambda i: {'Name': i[0], 'Value': i[1]},
            dimensions.items()
        )
    )


print(dims)


def lambda_func(a,b):
    return {'Name': a, 'Value': b}


if len(dimensions.keys()) > 0:
    dims = list()
    for x in dimensions.items():
        dims.append(lambda_func(x[0],x[1]))


print(dims)
