
def pre_order(binary_node, a_list):
    if binary_node:
        a_list.append(binary_node.val)
        pre_order(binary_node.left, a_list)
        pre_order(binary_node.right, a_list)


def in_order(binary_node, a_list):
    if binary_node:
        in_order(binary_node.left, a_list)
        a_list.append(binary_node.val)
        in_order(binary_node.right, a_list)


def post_order(binary_node, a_list):
    if binary_node:
        post_order(binary_node.right, a_list)
        a_list.append(binary_node.val)
        post_order(binary_node.left, a_list)


def bfs_iteration(binary_node):
    if binary_node:
        result_list = list()
        a_list = [binary_node]

        while len(a_list) > 0:
            temp_list = []
            for x in a_list:
                result_list.append(x.val)
                if x.left:
                    temp_list.append(x.left)
                if x.right:
                    temp_list.append(x.right)

            a_list = temp_list

        return result_list


def bfs_recursion(a_list, result_list):
    if a_list and len(a_list) > 0:
        temp_list = []
        for x in a_list:
            result_list.append(x.val)
            if x.left:
                temp_list.append(x.left)
            if x.right:
                temp_list.append(x.right)

        bfs_recursion(temp_list, result_list)

    return result_list
