
def compare_two_lists(a_list, b_list):
    if a_list is None:
        return False
    if b_list is None:
        return False
    if len(a_list) != len(b_list):
        return False

    for i in range(len(a_list)):
        if a_list[i] != b_list[i]:
            return False

    return True


def gcd(n, d):
    if n == d:
        return n
    if d > n:
        tmp = d
        d = n
        n = tmp

    while n%d != 0:
        r = n%d
        n = d
        d = r

    return d


def lcm(a, b):
    return (a*b)/gcd(a,b)


def _lcm(nums):
    nums.sort()
    _lcm = nums[0]
    for x in nums:
        _lcm = lcm(_lcm, x)

    return _lcm
