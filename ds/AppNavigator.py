import LinkedNode

'''
Implement App Navigator which supports
open()
close()
next(x)
previous(x)

Work In progress
'''

class AppNavigator:

    def __init__(self):
        self.head = None
        self.tail = None

    def open(self, app):
        if self.head is None:
            self.head = LinkedNode(app)
            self.tail = self.head
        else:
            temp = LinkedNode(app)
            temp.next = self.head
            self.head.prev = temp
            self.head = temp

    def close(self, app):
        if self.head is None:
            return
        else:
            pass

