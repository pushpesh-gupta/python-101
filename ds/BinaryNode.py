class BinaryNode:

    def __init__(self, val):
        self.val = val
        self.left = None
        self.right= None

    def __str__(self):
        return 'BinaryNode value is %s' % self.val
