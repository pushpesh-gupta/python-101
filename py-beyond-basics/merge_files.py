import glob2
from datetime import datetime

def files_merger(files, mergedfile):
    with open(mergedfile, "w") as wfile:
        for f in files:
            with open(f,"r") as rfile:
                wfile.write(rfile.read()+'\n')

mergedfile = datetime.strftime(datetime.now(), "%Y-%m-%d-%H-%M-%S-%f") + '.txt'

#files = ["file1.txt","file2.txt","file3.txt"]
files = glob2.glob("file*.txt")

files_merger(files, mergedfile)
