
def divide(a, b):
    try:
        return a/b
    except ZeroDivisionError:
        return "Invalid parameters"

print(divide(10,8))
print(divide(10,0))
