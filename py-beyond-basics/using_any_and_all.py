
lines  = ["trees are good", "pool is fresh", "farting is disgusting", "stupid fart is disgusting"]
bad_words = ["stupid", "fart", "disgust"]

for line in lines:
    if any(bad_word in line for bad_word in bad_words):
        print("Line -- %s -- has atleast one bad word" % (line))


for line in lines:
    if all(bad_word in line for bad_word in bad_words):
        print("Line -- %s -- has all the bad words" % (line))
