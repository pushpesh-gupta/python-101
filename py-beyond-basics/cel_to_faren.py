temps = [10, -20, -289, 100, 0]


def c_to_f(c):
    if c < -273.15:
        return "That temperature does not make sense"
    else:
        return c * (9/5) + 32


def writer(temps, filepath):
    with open(filepath, "w") as myfile:
        for t in temps:
            val = c_to_f(t)
            if type(val) == float:
                myfile.write(str(val) + '\n')


writer(temps, "example.txt")
