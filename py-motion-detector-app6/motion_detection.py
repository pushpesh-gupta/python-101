import cv2, time, pandas
from datetime import datetime

video = cv2.VideoCapture(0)

first_frame = None

status_list = [None, None]
times = []
df = pandas.DataFrame(columns=["StartTime", "EndTime"])

while True:
    #capture a frame (Note each frame is an n-D array for captured pixels)
    check, frame = video.read()

    status = 0

    #for efficiency convert frame to gray
    # This converts frame in to 2-D arrays for pixel colors
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # apply GaussianBlur to remove noise in the image. read online about it.
    gray = cv2.GaussianBlur(gray, (21, 21), 0)

    #capture firt frame. subsequent frames will be compared against it.
    if first_frame is None:
        first_frame = gray
        continue

    # compare current frame with the reference frame which is first frame
    delta_frame = cv2.absdiff(first_frame, gray)

    #Read online about the threshold method and THRESH_BINARY methodology
    thresh_frame = cv2.threshold(delta_frame, 30, 255, cv2.THRESH_BINARY)[1]

    #Read online about the dilate method
    thresh_frame = cv2.dilate(thresh_frame, None, iterations = 2)

    #make contours. Read online about them
    (cnts,_) = cv2.findContours(thresh_frame.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    for contour in cnts:
        #if a contour has less than 10k pixels skip.
        if cv2.contourArea(contour) < 10000:
            continue

        status=1
        (x, y, w, h)=cv2.boundingRect(contour)
        cv2.rectangle(frame, (x, y), (x+w, y+h), (0,255,0), 3)

    #so when status goes from 0 to 1, that means some moving object came in to the frame
    # and when status goes from 1 to 0, then moving object is out of the frame
    # so let's store the time when this transition happens

    status_list.append(status)
    status_list = status_list[-2:]

    if status_list[-1] == 1 and status_list[-2] == 0: #moving object came in
        times.append(datetime.now())
    if status_list[-1] == 0 and status_list[-2] == 1: # moving object went away
        times.append(datetime.now())


    cv2.imshow("First Frame",first_frame)
    cv2.imshow("Delta Frame",delta_frame)
    cv2.imshow("Threshold Frame",thresh_frame)
    cv2.imshow("Color Frame",frame)

    key = cv2.waitKey(1)
    if key == ord('q'):
        # capture end time, in case application is closed while moving object was in frame.
        # for this time is taken a last end time.
        if status == 1:
            times.append(datetime.now())
        break

for i in range(0, len(times), 2):
    df = df.append({"StartTime":times[i],"EndTime":times[i+1]}, ignore_index=True)

df.to_csv("Times.csv")

video.release()
cv2.destroyAllWindows
