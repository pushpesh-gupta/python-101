In this app we will try to detect motion via the camera of the laptop
Idea is to first capture a reference frame (with no person, everything still)
Then subsequent frames will be compared to this reference frame.
If any deltas are found, due to someone walking in to the frame
then we will store "in-time" and "out-time" for it.

Finally we will try to plot time series using bokeh library to capture time when a "moving object"
came in the frame.

Using python2
pip install opencv-python
pip install pandas
pip install bokeh
