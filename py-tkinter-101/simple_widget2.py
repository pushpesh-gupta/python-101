'''
Simple widget (desktop) app which can be used to convert KGs to grams, punds and ounces
'''

from tkinter import *

window = Tk()

def convert():
    kg_to_grams()
    kg_to_pounds()
    kg_to_ounces()

def kg_to_grams():
    miles = float(e1_value.get())*1000
    t1.insert(END, miles)

def kg_to_pounds():
    miles = float(e1_value.get())*2.20462
    t2.insert(END, miles)

def kg_to_ounces():
    miles = float(e1_value.get())*35.274
    t3.insert(END, miles)

e0 = Label(window, text='Kg')
e0.grid(row=0,column=0)

e1_value = StringVar()
e1 = Entry(window, textvariable = e1_value)
e1.grid(row=0, column=1)

b1 = Button(window, text = 'Convert', command = convert )
b1.grid(row=0,column=2)

t1 = Text(window, height=1, width=20)
t1.grid(row=1, column=0)

t2 = Text(window, height=1, width=20)
t2.grid(row=1, column=1)

t3 = Text(window, height=1, width=20)
t3.grid(row=1, column=2)

l0 = Label(window, text='grams')
l0.grid(row=2, column=0)

l1 = Label(window, text='pounds')
l1.grid(row=2, column=1)

l2 = Label(window, text='ounces')
l2.grid(row=2, column=2)


window.mainloop()
