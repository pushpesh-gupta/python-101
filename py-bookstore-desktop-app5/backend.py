import sqlite3

def drop_table():
    conn = sqlite3.connect("books.db")
    cur = conn.cursor()
    cur.execute("DROP TABLE IF EXISTS books")
    conn.commit()
    conn.close()

def create_table():
    conn = sqlite3.connect("books.db")
    cur = conn.cursor()
    cur.execute("CREATE TABLE IF NOT EXISTS books(id INTEGER PRIMARY KEY, title text, author text, year integer, isbn integer)")
    conn.commit()
    conn.close()

def insert(title, author, year, isbn):
    conn = sqlite3.connect("books.db")
    cur = conn.cursor()
    cur.execute("INSERT INTO books VALUES (NULL,?,?,?,?)", (title, author, year, isbn))
    conn.commit()
    conn.close()

def delete(id):
    conn = sqlite3.connect("books.db")
    cur = conn.cursor()
    print(id)
    print(type(id))
    cur.execute("DELETE FROM books where id = ?", (id,))
    conn.commit()
    conn.close()

def update(id, title=None, author=None, year=None, isbn=None):
    conn = sqlite3.connect("books.db")
    cur = conn.cursor()
    print("id %s, title %s, author %s, year %s, isbn %s" % (id, title, author, year, isbn ))
    if title is not None:
        cur.execute("UPDATE books set title = ? WHERE id = ?", (title, id))
    if author is not None:
        cur.execute("UPDATE books set author = ? WHERE id = ?", (author, id))
    if year is not None:
        cur.execute("UPDATE books set year = ? WHERE id = ?", (year, id))
    if isbn is not None:
        cur.execute("UPDATE books set isbn = ? WHERE id = ?", (isbn, id))
    conn.commit()
    conn.close()

def view():
    conn = sqlite3.connect("books.db")
    cur = conn.cursor()
    cur.execute("SELECT * from books")
    rows = cur.fetchall()
    conn.close()
    return rows

def search(title=None, author=None, year=None, isbn=None):
        conn = sqlite3.connect("books.db")
        cur = conn.cursor()
        cur.execute("SELECT * from books where title = ? OR author = ? OR year = ? OR isbn = ?" , (title, author, year, isbn))
        rows = cur.fetchall()
        conn.close()
        return rows

create_table()

#drop_table()
# create_table()
# insert("py-101","gupta", 2018, 00000)
# insert("py-102","john", 2019, 123789)
# print(view())
# #print(search(title="py-101"))
# update(2, title="py-201")
#print(view())
