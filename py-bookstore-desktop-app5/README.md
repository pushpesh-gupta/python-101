Bookstore desktop app

Needs sqlite3 and Tkinter python packages.
Using python2 and sqlite3 install on this machine does not works with pyhton3

Create python2 virtual env
virtualenv .virtualenvs/py2venv

Features:
Add book entry
Delete book entry
Update book entry
View All book entries
Search for a book entry

To convert it into a distribution of a desktop app, run below:
pip install pyinstaller
pyinstaller --onefile --windowed frontend.py
