import folium
import pandas

# Create base world map layer using folium

map = folium.Map(location=[38.58, -99.09], zoom_start=6, tiles="Mapbox Bright")

# Read Volcanoes data and create FeatureGroup and then and that feature group as "child" on top of base map

data = pandas.read_csv("Volcanoes.txt")

lat = list(data["LAT"])
lon = list(data["LON"])
elev = list(data["ELEV"])
names = list(data["NAME"])

html = """
Volcano name:<br>
<a href="https://www.google.com/search?q=%%22%s%%22" target="_blank">%s</a><br>
Height: %s m
"""

def color_producer(el):
    if el < 1000:
        return 'green'
    elif 1000 <= el < 3000:
        return 'orange'
    else:
        return 'red'

fgv = folium.FeatureGroup(name = "Volcanoes")

for lt, ln, el, name in zip(lat, lon, elev, names):
    iframe = folium.IFrame(html=html % (name, name, el), width=200, height=100)
    #fg.add_child(folium.Marker(location=[lt, ln], popup=folium.Popup(iframe), icon = folium.Icon(color=color_producer(el))))
    fgv.add_child(folium.CircleMarker(location=[lt,ln], radius = 6, popup=folium.Popup(iframe), fill_color=color_producer(el)
    , color = 'grey', fill_capacity=0.7))

map.add_child(fgv)

# Read world.json file which has GeoJson data
# From it read population data and color code countries based on large, medium, low Population
# Add this feature group to base map

fgp = folium.FeatureGroup(name = "Population")

fgp.add_child(folium.GeoJson(data=open('world.json', 'r', encoding='utf-8-sig').read(),
style_function=lambda x: {'fillColor':'green' if x['properties']['POP2005'] < 10000000
else 'orange' if 10000000 <= x['properties']['POP2005'] < 100000000 else 'red'}))

map.add_child(fgp)

# use folium.LayerControl() to add toggle feature on the map to pick volcanoes or population features

map.add_child(folium.LayerControl())
map.save("Map.html")
