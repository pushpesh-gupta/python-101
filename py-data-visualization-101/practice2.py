
# read weather data from http://pythonhow.com/data/verlegenhuken.xlsx and plot Year vs Engineering graph


from bokeh.plotting import figure
from bokeh.io import output_file, show
import pandas

df = pandas.read_excel("http://pythonhow.com/data/verlegenhuken.xlsx",sheet_name=0)

x = df["Temperature"]/10
y = df["Pressure"]/10

#create a figure object
p = figure(plot_width=400, plot_height=400, tools='pan')

p.title.text="Temperature and Air Pressure"
p.title.text_color="Gray"
p.title.text_font="times"
p.title.text_font_style="bold"
p.xaxis.minor_tick_line_color=None
p.yaxis.minor_tick_line_color=None
p.xaxis.axis_label="Temperature (°C)"
p.yaxis.axis_label="Pressure (hPa)"

#create line plot
p.circle(x,y, size=0.5, color="blue", alpha=0.5)

#prepare the output file
output_file("Practice2.html")

show(p)
