from bokeh.plotting import figure
from bokeh.io import output_file, show

#sample data
x = [1,2,3,4,5,6]
y = [6,5,4,3,2,1]

#prepare the output file
output_file("Line1.html")

#create a figure object
f = figure()

#create line plot
f.circle(x,y)

show(f)
