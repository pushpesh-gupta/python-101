from bokeh.plotting import figure
from bokeh.io import output_file, show
import pandas

#read sample data from a csv
df = pandas.read_csv("data.csv")
x = df["x"]
y = df["y"]

#prepare the output file
output_file("Line2.html")

#create a figure object
f = figure()

#create plot using circle for each data point.
#even size of each data-circle can be controlled
f.circle(x,y, size = [i*2.0 for i in y], color="red", alpha=0.5)

show(f)
