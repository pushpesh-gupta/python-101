
# read bachelors.csv and plot Year vs Engineering graph


from bokeh.plotting import figure
from bokeh.io import output_file, show
import pandas

df = pandas.read_csv("bachelors.csv")

x = df["Year"]
y = df["Engineering"]

#prepare the output file
output_file("Practice1.html")

#create a figure object
p = figure(plot_width=400, plot_height=400, tools='pan')

p.title.text="Engineering graduates by year"
p.title.text_color="Gray"
p.title.text_font="times"
p.title.text_font_style="bold"
p.xaxis.minor_tick_line_color=None
p.yaxis.minor_tick_line_color=None
p.xaxis.axis_label="Year"
p.yaxis.axis_label="Students"

#create line plot
p.line(x,y, line_color='green')

show(p)
