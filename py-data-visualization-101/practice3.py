
# plot time series data

from bokeh.plotting import figure
from bokeh.io import output_file, show
import pandas

df = pandas.read_csv("adbe.csv", parse_dates=["Date"])

#create a figure object
p = figure(plot_width=500, plot_height=500, x_axis_type = 'datetime')

#create line plot
p.line(df["Date"],df["Close"], color="black", alpha=0.5)
p.line(df["Date"],df["Open"], color="green", alpha=0.5)
p.line(df["Date"],df["High"], color="blue", alpha=0.5)
p.line(df["Date"],df["Low"], color="red", alpha=0.5)

#prepare the output file
output_file("Practice3.html")

show(p)
