import sqlite3

class Backend:

    def __init__(self, db):
        self.conn = sqlite3.connect(db)
        self.cur = self.conn.cursor()
        self.cur.execute("CREATE TABLE IF NOT EXISTS books(id INTEGER PRIMARY KEY, title text, author text, year integer, isbn integer)")
        self.conn.commit()
        # self.conn.close()

    def drop_table(self):
        self.cur.execute("DROP TABLE IF EXISTS books")
        self.conn.commit()

    def insert(self, title, author, year, isbn):
        self.cur.execute("INSERT INTO books VALUES (NULL,?,?,?,?)", (title, author, year, isbn))
        self.conn.commit()

    def delete(self, id):
        self.cur.execute("DELETE FROM books where id = ?", (id,))
        self.conn.commit()

    def update(self, id, title=None, author=None, year=None, isbn=None):
        if title is not None:
            self.cur.execute("UPDATE books set title = ? WHERE id = ?", (title, id))
        if author is not None:
            self.cur.execute("UPDATE books set author = ? WHERE id = ?", (author, id))
        if year is not None:
            self.cur.execute("UPDATE books set year = ? WHERE id = ?", (year, id))
        if isbn is not None:
            self.cur.execute("UPDATE books set isbn = ? WHERE id = ?", (isbn, id))
        self.conn.commit()

    def view(self):
        self.cur.execute("SELECT * from books")
        rows = self.cur.fetchall()
        return rows

    def search(self,title=None, author=None, year=None, isbn=None):
        self.cur.execute("SELECT * from books where title = ? OR author = ? OR year = ? OR isbn = ?" , (title, author, year, isbn))
        rows = self.cur.fetchall()
        return rows

    def __del__(self):
        self.conn.close()
