import json
import sys
from difflib import get_close_matches

dictionary = json.load(open("data.json"))
# print("size of dictionary is %s KBs" % (sys.getsizeof(dictionary)/1024))

def meaning(word):
    if word.lower() in dictionary:
        on_match(word.lower())
    elif word.title() in dictionary:
        on_match(word.title())
    elif word in dictionary:
        on_match(word)
    elif word.upper() in dictionary:
        on_match(word.upper())
    else:
        closest_match = give_suggestion(word.lower())
        if closest_match is not None:
            print("Word %s does not exist in the dictionary. Did you mean %s?" % (word, closest_match))
            _input = input("Enter Y or N: ")
            if _input.lower() == "y":
                return meaning(closest_match)
            print("Word %s does not exist in the dictionary. Please try again or press crtl+C to exit" % (word))
        else:
            print("Word %s does not exist in the dictionary. Please try again or press crtl+C to exit" % (word))

def on_match(word):
    result = ""
    for m in dictionary[word]:
        result = result + '\n' + m
    print(result)

def give_suggestion(word):
    closest_matches = get_close_matches(word,dictionary.keys())
    if closest_matches is not None:
        return closest_matches[0]

def take_user_input():
    while True:
            try:
                print("####################")
                _word = input("Enter a word: ")
                meaning(_word)
            except KeyboardInterrupt:
                print("\n Bye for now!")
                exit()

print("Welcome to interactive dictionary. Enter a word or press crtl+C to exit ")
take_user_input()
