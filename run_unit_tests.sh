#!/bin/bash

echo "starting unit tests..."

python3 -m unittest discover -s ./tests -p test_*.py

echo "Done running unit tests."
